<?php
/**
 * Footer Template
 *
 * @package Ascend theme
 */

			do_action( 'ascend_after_content' ); ?>
			</div><!-- /.wrap -->
			<?php
			do_action( 'ascend_footer' );
			?>
		</div><!--Wrapper-->
		<?php wp_footer(); ?>
	</body>
	<script>
	    jQuery('.cert-btn a').attr("download","");
	</script>
</html>
